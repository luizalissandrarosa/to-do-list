<div align="center" id="top">

<img width="50px" src="./assets/images/pressure_pin.png" />

</div>

&#xa0;

<div align="center">

<p>
  <a href="#dart-about"> About </a> &#xa0; | &#xa0; 
  <a href="#computer-technologies-used"> Technologies used </a> &#xa0; | &#xa0; 
  <a href="#white_check_mark-requirements"> Requirements </a> &#xa0; | &#xa0; 
  <a href="#checkered_flag-starting"> Starting </a> &#xa0; | &#xa0; 
  <a href="#bulb-roadmap"> Roadmap </a> &#xa0; | &#xa0;
  <a href="#unlock-license"> License </a> &#xa0; | &#xa0;
  <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Developer </a>
</p>

</div>

<br>

## :dart: About ##

To-do list software. Add your tasks and organize your day to day. This project was proposed in the <a href='https://ejcm.com.br/'> EJCM </a> selection process.

<img src="to-do.PNG" />


## :computer: Technologies used ##

- [HTML](https://www.w3schools.com/html/)
- [CSS](https://developer.mozilla.org/pt-BR/docs/Web/CSS)

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) installed.

## :checkered_flag: Starting ##

```bash
# Clone the folder.
$ git clone https://gitlab.com/luizalissandrarosa/to-do-list/

# Access it.
$ cd to-do-list

```

## :bulb: Roadmap ##

- [ ] Responsiveness.

## :unlock: License ##

This project is under license from MIT. To learn more, see [LICENSE](LICENSE). 

&#xa0;

<hr/>

Developed with ❤ by <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Luiza Lissandra :rocket: </a>

<a href="#top"> Back to top </a>
